#ifndef CLI_CHANNEL
#define CLI_CHANNEL

#include "Channel.h"

#include <memory>
#include <string>

#include "SeverityLevel.h"

class CliChannel : public Channel
{
public:
	void sendMessage(const Notification notification_,
					 const struct Receiver receiver_,
					 const bool waitForResponse_ = true) override;
	std::shared_ptr<Response> getResponse() const override;

	virtual bool hasResponse() const override;
	virtual void clearResponse() override;

	void printNotification(const Notification notification_, const struct Receiver receiver_) const;
	void waitForResponse(const struct Receiver receiver_);

protected:
	std::string toString(const SeverityLevel severity) const;

private:
	std::shared_ptr<Response> response;
};

#endif