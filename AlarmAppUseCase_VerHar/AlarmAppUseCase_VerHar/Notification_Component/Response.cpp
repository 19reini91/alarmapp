#include "Response.h"

Response::Response(const time_t timestamp_, const bool positive_)
	: timestamp(timestamp_)
	, positive (positive_ )
{
}

time_t Response::getTimestamp() const
{
	return timestamp;
}

bool Response::isPositive() const
{
	return positive;
}
