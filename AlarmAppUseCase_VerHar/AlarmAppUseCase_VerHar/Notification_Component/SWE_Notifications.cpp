#include <thread>

#include "../Notification_Component/BasicNotifier.h"
#include "../Notification_Component/CliChannel.h"
#include "../Notification_Component/Notification.h"

int main()
{
	std::shared_ptr<Channel> channel = std::make_shared<CliChannel>();

	BasicNotifier notifier(channel, "Notification_Component/notification_config.txt", 10U);
	notifier.notify(Notification("Omi is umpfoin", SeverityLevel::ALARM));

	std::this_thread::sleep_for(std::chrono::seconds(100));

	return 0;
}