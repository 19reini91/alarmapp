#include "BasicNotifier.h"

#include <chrono>
#include <iostream>
#include <fstream>
#include <thread>

#include "Notification.h"
#include "Receiver.h"
#include "Response.h"

BasicNotifier::BasicNotifier(std::shared_ptr<Channel> channel_, 
							 const std::string& configFilePath_, 
						     const unsigned int timeout_sec_ )
	: channel(channel_)
	, configFilePath(configFilePath_)
    , timeout_sec(timeout_sec_)
{
	readConfigFile(configFilePath);
}

void BasicNotifier::notify(const Notification notification) const
{
   std::thread fred( &BasicNotifier::handleNotification, this, notification );
   fred.detach();
}

void BasicNotifier::refreshConfig()
{
	readConfigFile(configFilePath);
}

void BasicNotifier::setReceiverFromLine(const std::string& line_)
{
	size_t startName    = line_.find('=') + 1;
	size_t startAddress = line_.find(';') + 1;

	size_t lenName = startAddress - startName - 1;

	std::string receiver    = line_.substr(0, startName - 1  );
	std::string name        = line_.substr(startName, lenName);
	std::string addressInfo = line_.substr(startAddress      );

	if      (receiver == "primary_receiver")
		primaryReceiver   = Receiver(name, addressInfo);
	else if (receiver == "secondary_receiver")
		secondaryReceiver = Receiver(name, addressInfo);
}

void BasicNotifier::notifyPrimaryReceiver(const Notification notification_) const
{
	channel->sendMessage(notification_, primaryReceiver, true);
}

void BasicNotifier::notifySecondaryReceiver(const Notification notification_) const
{
	channel->sendMessage(notification_, secondaryReceiver, false);
}

void BasicNotifier::handleNotification( const Notification notification_ ) const
{
	notifyPrimaryReceiver(notification_);
	std::this_thread::sleep_for( std::chrono::seconds( timeout_sec ) );
	if (channel->hasResponse())
	{
		std::cout << "Response received from primary receiver: " << std::endl;
		std::shared_ptr<Response> response = channel->getResponse();
		std::cout << response->isPositive() << " at time "
			<< response->getTimestamp() << std::endl;
		channel->clearResponse();
	}
	else
	{
		std::cout << std::endl 
			 << "No response received from primary receiver." << std::endl;
		notifySecondaryReceiver(notification_);
	}
}

void BasicNotifier::readConfigFile(const std::string& configFilePath_)
{
	std::ifstream configFile(configFilePath_);
   if (!configFile.is_open())
   {
      std::cout << "Unable to open " << configFilePath_ <<"." << std::endl;
      return;
   }
	while (!configFile.eof())
	{
		std::string line;
		std::getline(configFile, line);

		setReceiverFromLine(line);
	}
	configFile.close();
}
