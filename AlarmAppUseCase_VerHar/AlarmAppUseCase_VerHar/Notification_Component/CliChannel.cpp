#include "CliChannel.h"

#include <iostream>
#include <ctime>
#include <thread>

#include "Notification.h"
#include "Receiver.h"
#include "Response.h"

void CliChannel::sendMessage(const Notification notification_, 
							 const struct Receiver receiver_, 
							 const bool waitForResponse_ /* = true*/)
{
	printNotification(notification_, receiver_);

   if (waitForResponse_)
   {
      std::thread waitingThread( &CliChannel::waitForResponse, this, receiver_ );
      waitingThread.detach();
   }
}

std::shared_ptr<Response> CliChannel::getResponse() const
{
	return response;
}

void CliChannel::printNotification(const Notification notification_, const Receiver receiver_) const
{
	std::cout << "Notification to " << receiver_.name 
		<< " at address " <<receiver_.addressInformation << ": " << std::endl
		<< "\t" << toString(notification_.getSeverity()) << ": " 
		<< notification_.getText() << std::endl;
}

void CliChannel::waitForResponse( const struct Receiver receiver_ )
{
	bool isPositive{ false };
	std::string command;

   std::cout << receiver_.name << ": ";
	std::cin >> command;

	time_t timestamp = std::time(0);
	isPositive = (command == "J");

	response = std::make_shared<Response>(Response(timestamp, isPositive));
}

std::string CliChannel::toString(const SeverityLevel severity) const
{
	switch (severity)
	{
		case SeverityLevel::INFO:    return "INFO";    break;
		case SeverityLevel::WARNING: return "WARNING"; break;
		case SeverityLevel::ALARM:   return "ALARM";   break;
		default:				     return "UNKNOWN";
	}
}

bool CliChannel::hasResponse() const
{
	return response != nullptr;
}

void CliChannel::clearResponse()
{
	response = nullptr;
}
