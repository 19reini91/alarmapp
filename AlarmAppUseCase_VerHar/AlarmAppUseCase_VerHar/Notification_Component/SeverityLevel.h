#ifndef SEVERITY_LEVEL
#define SEVERITY_LEVEL

enum SeverityLevel
{
	INFO = 0,
	WARNING,
	ALARM,
};

#endif