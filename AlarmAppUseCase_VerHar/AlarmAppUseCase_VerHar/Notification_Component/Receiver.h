#ifndef RECEIVER
#define RECEIVER

#include <string>

struct Receiver
{
	Receiver() = default;
	/*
	@param name_ name of receiver
	@param addressInformation_ address of receiver
	*/
	Receiver(const std::string& name_, const std::string& addressInformation_)
		: name(name_), addressInformation(addressInformation_) {}

	std::string name;
	std::string addressInformation;
};

#endif // !RECEIVER