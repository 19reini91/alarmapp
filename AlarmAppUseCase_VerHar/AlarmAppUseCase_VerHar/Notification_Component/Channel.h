#ifndef CHANNEL
#define CHANNEL

#include <memory>

class Notification;
struct Receiver;
class Response;

class Channel
{
public:
	virtual void sendMessage(const Notification notification_,
		const struct Receiver receiver_,
		const bool waitForResponse = true) = 0;

	virtual std::shared_ptr<Response> getResponse() const = 0;

	virtual bool hasResponse() const = 0;
	virtual void clearResponse() = 0;
};

#endif