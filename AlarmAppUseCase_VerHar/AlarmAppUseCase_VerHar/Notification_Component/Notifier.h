#ifndef NOTIFIER
#define NOTIFIER

class Notification;

class Notifier
{
public:
	/*
	sends Notification to internally specified Receiver via the
	internally specified channel
	*/
	virtual void notify(const Notification notification_) const = 0;
};


#endif


