#ifndef BASIC_NOTIFIER
#define BASIC_NOTIFIER

#include <memory>
#include <string>

#include "Notifier.h"
#include "Channel.h"
#include "Receiver.h"

class Notification;

class BasicNotifier : public Notifier
{
public:
	/*
	@param channel_ pointer to channel object to be used for transmitting notifications
	@param configFilePath_ path to configuration file, which specifies receivers
	@param timeout_sec_ if not response is detected within this time,
						the secondary reciever is contacted
	*/
	BasicNotifier(std::shared_ptr<Channel> channel_, 
				  const std::string& configFilePath_, 
				  const unsigned int timeout_sec_ = 30);
	BasicNotifier() = delete;

	/*
	sends Notification first to primary receiver,
	if no response is detected after the timeout the Notification
	is then sent to the secondary receiver
	*/
	void notify(const Notification notification) const;

	/*
	reads content of specified configuration file
	should be called, when configuration file is changed
	*/
	void refreshConfig();

protected:
	void handleNotification(const Notification notification) const;
	void readConfigFile(const std::string& configFilePath_);
	void setReceiverFromLine(const std::string& line_);

	void notifyPrimaryReceiver(const Notification notification) const;
	void notifySecondaryReceiver(const Notification notificaiton) const;

private:
	std::shared_ptr<Channel> channel;

	Receiver primaryReceiver;
	Receiver secondaryReceiver;

	std::string configFilePath;
	unsigned int timeout_sec;
};

#endif