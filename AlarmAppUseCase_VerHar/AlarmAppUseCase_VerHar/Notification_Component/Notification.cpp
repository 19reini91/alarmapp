#include "Notification.h"

Notification::Notification(const std::string& text_, const SeverityLevel severity_)
	: text    (text_    )
	, severity(severity_)
{
}

std::string Notification::getText() const
{
	return text;
}

SeverityLevel Notification::getSeverity() const
{
	return severity;
}