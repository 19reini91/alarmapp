#ifndef RESPONSE
#define RESPONSE

#include <time.h>

class Response
{
public:
	Response(const time_t timestamp_, const bool positive_);
	Response() = delete;

	time_t getTimestamp() const;
	bool isPositive() const;

private:
	const time_t timestamp;
	const bool positive;
};

#endif