#ifndef NOTIFICATION
#define NOTIFICATION

#include <string>

#include "SeverityLevel.h"

class Notification
{
public:
	/*
	@param text_ notification message
	@param severity_ severity of incident
	*/
	Notification(const std::string& text_, const SeverityLevel severity_);
	Notification() = delete;

	std::string getText() const;
	SeverityLevel getSeverity() const;

protected:
	const std::string text;
	const SeverityLevel severity;
};

#endif