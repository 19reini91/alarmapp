#include "pch.h"
#include "CppUnitTest.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/BasicNotifier.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/Channel.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/CliChannel.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/Notification.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/Notifier.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/Receiver.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/Response.h"
#include "../AlarmAppUseCase_VerHar/Notification_Component/SeverityLevel.h"
#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(UnitTests)
	{
	public:
		
		TEST_METHOD(responseIsPositiveFlag)
		{
			const bool isPositiveFlag = true;
			Response test(time(NULL), isPositiveFlag);
			Assert::AreEqual(isPositiveFlag, test.isPositive());
		}

		TEST_METHOD(notificationCheckText)
		{
			std::string text = "test";
			Notification notification(text, SeverityLevel::ALARM);
			Assert::AreEqual(text, notification.getText());
		}

		TEST_METHOD(receiverCheckAdressInformation)
		{
			std::string receiverName = "receiver";
			std::string addressInformation = "addressInformation";
			Receiver receiver(receiverName, addressInformation);

			Assert::AreEqual(addressInformation, receiver.addressInformation);
		}

		TEST_METHOD(receiverCheckName)
		{
			std::string receiverName = "receiver";
			std::string addressInformation = "addressInformation";
			Receiver receiver(receiverName, addressInformation);

			Assert::AreEqual(receiverName, receiver.name);
		}
	};
}
