/*
 * iconfig.h
 *
 */

#ifndef INTERFACES_ICONFIG_H_
#define INTERFACES_ICONFIG_H_


struct IConfig
{
    bool (*SetPassword)(char *);
    bool (*SetDisarmSnooze)(int);
    bool (*SetArmSnooze)(int);

    int (*GetArmSnooze)();
    int (*GetDisarmSnooze)();
} IConfig;


#endif /* INTERFACES_ICONFIG_H_ */
