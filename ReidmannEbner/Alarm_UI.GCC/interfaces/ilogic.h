/*
 * ilogic.h
 *
 */

#ifndef INTERFACES_ILOGIC_H_
#define INTERFACES_ILOGIC_H_


struct ILogic
{
	bool (*CheckConnection)();
    bool (*ArmSystem)(char *password);
    bool (*ArmSnooze)(char *password);
    bool (*DisarmSystem)(char *password);
    bool (*DisarmSnooze)(char *password);
    bool (*CheckMotion)(void);
    bool (*CheckDisarm)(void);
    bool (*CheckArmSnooze)(void);
    bool (*CheckDisarmSnooze)(void);
    bool (*CleanupTimers)(char *password);
} ILogic;


#endif /* INTERFACES_ILOGIC_H_ */
