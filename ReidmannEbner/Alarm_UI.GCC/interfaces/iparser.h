/*
 * parser.h
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#ifndef INTERFACE_PARSER_H_
#define INTERFACE_PARSER_H_

#include <stdbool.h>

#include "icommandhandler.h"

struct iParser{
	bool (*ParseInput)(char* input);
}iParser;



#endif /* INTERFACE_PARSER_H_ */
