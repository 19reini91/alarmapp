/*
 * main.c
 *
 */

/* standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>
#include <time.h>

#include "interfaces/iconfig.h"
#include "interfaces/ilogic.h"
#include "interfaces/iui.h"
#include "interfaces/iuserComm.h"

#include "application/application.h"

#include "helpers/kbhit.h"

#define REDIRECTERROR //Comment this line to show error messages, otherwise they are stored in errors.txt

#define MESSAGELENGTH 256

/* Echo Communication Messages to console*/
bool g_verbose = false;

int main(int argc, char *argv[])
{

#ifdef REDIRECTERROR
	FILE* error_file = freopen("alarmerrors.txt", "w", stderr); // <- redirects error messages to errors.txt"
	time_t current_time = time(NULL);
	fprintf(error_file, "----------------%s------------- \n \n \n", ctime(&current_time));
#endif //REDIRECTERROR

	struct ILogic *logic = NULL;
	struct IConfig *config = NULL;
	struct iUI *ui  = NULL;
	struct iuserComm *userComm = NULL;

	char msg[MESSAGELENGTH];
	char timemsg[100];

	if(!ApplicationFactory("http://localhost:8080/shapi/", &logic, &config, &ui, &userComm)){
		return -1;
	}

	char *password = "swordfish";

    if(!logic->CheckConnection()){
    	ui->ShowMessage("You have to start FREDOSAR!\n ");
    	ui->ShowMessage("Waiting for FREDOSAR ... \n");
    	userComm->ShowFailedFredosarConnection();
        do{
        	sleep(1);
        }while(!logic->CheckConnection());
        ui->ShowMessage("Connected!");
    }
    else{
    	ui->ShowMessage("Connected to FREDOSAR!\n ");
    }


    ui->ShowMessage("Configuring System");
	logic->CleanupTimers(password);
	config->SetPassword(password);
	config->SetArmSnooze(5);
	config->SetDisarmSnooze(5);


	ui->ShowWelcomeMessage();
	bool alarm_started = false;
	bool quit_requested = false;


	do{
		ui->ReadUserInput();
		alarm_started = ui->AlarmStartRequested();
		quit_requested = ui->QuitRequested();
	}while(!alarm_started && !quit_requested);

	if(quit_requested){
		DisposeApplication();


#ifdef REDIRECTERROR
		fclose(error_file);
#endif //REDIRECTERROR


		return 0;
	}

	snprintf(msg, MESSAGELENGTH - 1, "Arming system (%ds) .. Press Button to stop arming", config->GetArmSnooze());
    ui->ShowMessage(msg);

	//Arming System
	logic->ArmSnooze(password);

	bool bDisarmed = false;


	do {

		if (logic->CheckDisarm()){
			bDisarmed = logic->DisarmSystem(password);
		    break;
		}

		//sleep 250ms
		usleep(250000);
		if(!logic->CheckConnection){
			ui->ShowMessage("Connection lost");
		}

	} while (!logic->CheckArmSnooze());

	if (!bDisarmed) {

			logic->ArmSystem(password);
			ui->ShowMessage("Armed system ...\n");

			time_t armedStartTime;
			armedStartTime=time(NULL);

		    bool bMotion = false;
		    bool bDisarmSnooze = false;


			//Disarming System (if necessary and not already disarmed)
			do {

			if(!bMotion){
				if(difftime(time(NULL), armedStartTime) > 5){
					ui->ShowMessage("Attention - No movement detected for over 3 hours\n");
					armedStartTime = time(NULL);
					logic->DisarmSystem(password);
					break;
				}
				bMotion = logic->CheckMotion();
			}

			if(!bMotion){
				bMotion = logic->CheckMotion();
			}

			if (bMotion && !bDisarmSnooze){
				bDisarmSnooze = logic->DisarmSnooze(password);
				snprintf(msg, MESSAGELENGTH - 1, "Disarming system! snooze (%ds)", config->GetDisarmSnooze());
			    ui->ShowMessage(msg);
			}

			if (logic->CheckDisarm()){
				bDisarmed = logic->DisarmSystem(password);
			    break;
			}

			//sleep 250ms
			usleep(250000);

		} while (!bMotion || !logic->CheckDisarmSnooze());

		if (!bDisarmed)
		{
		    time_t ltime; /* calendar time */
		    ltime=time(NULL); /* get current cal time */
		    snprintf(timemsg,100,"ALARM at %s\n",asctime( localtime(&ltime) ));
			ui->ShowMessage("Alarm!!!!!");
			ui->ShowMessage(timemsg);
		}
		else
	    	ui->ShowMessage("Disarmed system ...\n");
	}
	else {
		ui->ShowMessage("Disarmed system ...\n");
	}


	logic->CleanupTimers(password);

	ui->ShowMessage("Terminating App ...\n");
	//ui->ShowMessage("No Motion will \n");

	DisposeApplication();



#ifdef REDIRECTERROR
	fclose(error_file);
#endif //REDIRECTERROR


    return 0;
}

