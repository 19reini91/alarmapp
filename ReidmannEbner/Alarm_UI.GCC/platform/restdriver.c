/*
 * restdriver.c

 *
 */

/* standard includes */
#include "../platform/restdriver.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

/* json-c (https://github.com/json-c/json-c) */
#include <json-c/json.h>

/* libcurl (http://curl.haxx.se/libcurl/c) */
#include <curl/curl.h>

#include "../global_defines.h"
#include "../interfaces/inetcom.h"

//Forward Declarations
static char *base_url = NULL;
static json_object *SendMessageInternal(char *msg, char *request, json_object *json_post);
static void SetBaseURLInternal(char *url);

//Implementation
struct INetCom * newINetCom(void)
{
	struct INetCom *newStruct = malloc(sizeof(struct INetCom));
    if(newStruct == NULL)
    	return NULL;

    newStruct->SendMessage = SendMessageInternal;
    newStruct->SetBaseURL = SetBaseURLInternal;

    return newStruct;
}


/* holder for curl fetch */
struct curl_fetch_st {
    char *payload;
    size_t size;
};

/* callback for curl fetch */
size_t curl_callback (void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;                             /* calculate buffer size */
    struct curl_fetch_st *p = (struct curl_fetch_st *) userp;   /* cast pointer to fetch struct */

    /* expand buffer */
    p->payload = (char *) realloc(p->payload, p->size + realsize + 1);

    /* check buffer */
    if (p->payload == NULL) {
      /* this isn't good */
      fprintf(stderr, "ERROR: Failed to expand buffer in curl_callback\n");
      /* free buffer */
      free(p->payload);
      /* return */
      return -1;
    }

    /* copy contents to buffer */
    memcpy(&(p->payload[p->size]), contents, realsize);

    /* set new buffer size */
    p->size += realsize;

    /* ensure null termination */
    p->payload[p->size] = 0;

    /* return size */
    return realsize;
}

/* fetch and return url body via curl */
CURLcode curl_fetch_url(CURL *ch, const char *url, struct curl_fetch_st *fetch) {
    CURLcode rcode;                   /* curl result code */

    /* init payload */
    fetch->payload = (char *) calloc(1, sizeof(fetch->payload));

    /* check payload */
    if (fetch->payload == NULL) {
        /* log error */
        fprintf(stderr, "ERROR: Failed to allocate payload in curl_fetch_url");
        /* return error */
        return CURLE_FAILED_INIT;
    }

    /* init size */
    fetch->size = 0;

    /* set url to fetch */
    curl_easy_setopt(ch, CURLOPT_URL, url);

    /* set calback function */
    curl_easy_setopt(ch, CURLOPT_WRITEFUNCTION, curl_callback);

    /* pass fetch struct pointer */
    curl_easy_setopt(ch, CURLOPT_WRITEDATA, (void *) fetch);

    /* set default user agent */
    curl_easy_setopt(ch, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    /* set timeout */
    curl_easy_setopt(ch, CURLOPT_TIMEOUT, 5);

    /* enable location redirects */
    curl_easy_setopt(ch, CURLOPT_FOLLOWLOCATION, 1);

    /* set maximum allowed redirects */
    curl_easy_setopt(ch, CURLOPT_MAXREDIRS, 1);

    /* fetch the url */
    rcode = curl_easy_perform(ch);

    /* return */
    return rcode;
}


static json_object *SendMessageInternal(char *msg, char *request, json_object *json_post)
{
    CURL *ch;                                               // curl handle
    CURLcode rcode;                                         // curl result code

    struct curl_fetch_st curl_fetch;                        // curl fetch struct
    struct curl_fetch_st *cf = &curl_fetch;                 // pointer to fetch struct
    struct curl_slist *headers = NULL;                      // http headers to send with request

    json_object *json = NULL;
    enum json_tokener_error jerr = json_tokener_success;

    if (base_url == NULL || msg == NULL || request == NULL){
        fprintf(stderr, "ERROR: BASE-URL, MSG and Request type (GET, POST, PUT, DELETE) must be given!\n");
        return NULL;
    }

    // init curl handle
    if ((ch = curl_easy_init()) == NULL) {
        fprintf(stderr, "ERROR: Failed to create curl handle in fetch_session\n");
        return NULL;
    }

    // set content type
    headers = curl_slist_append(headers, "Accept: application/json");
    headers = curl_slist_append(headers, "Content-Type: application/json");

	// set curl options
    if (json_post != NULL && !strcmp(request, "POST"))
    {
		curl_easy_setopt(ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_easy_setopt(ch, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(ch, CURLOPT_POSTFIELDS, json_object_to_json_string(json_post));
    }
    else
    {
		curl_easy_setopt(ch, CURLOPT_CUSTOMREQUEST, request);
		curl_easy_setopt(ch, CURLOPT_HTTPHEADER, headers);
    }

    size_t buff_size = strlen(base_url) + strlen(msg) + 1;
    char *url = malloc(buff_size);

    strcpy(url, base_url);
    strcat(url, msg);

    // fetch page and capture return code
    rcode = curl_fetch_url(ch, url, cf);

    // cleanup curl handle
    curl_easy_cleanup(ch);

    // free headers
    curl_slist_free_all(headers);


    // check return code
    if (rcode != CURLE_OK) {
        fprintf(stderr, "ERROR: Failed to fetch url (%s) - curl said: %s\n",
            url, curl_easy_strerror(rcode));
        free(url);
        return NULL;
    }

    free(url);

    // check payload
    if (cf->payload != NULL) {
    	if (*cf->payload != 0)
    		json = json_tokener_parse_verbose(cf->payload, &jerr);
        free(cf->payload);
    } else {
        fprintf(stderr, "ERROR: Failed to populate payload\n");
        free(cf->payload);
        return NULL;
    }

    // check error
    if (jerr != json_tokener_success) {
        fprintf(stderr, "ERROR: Failed to parse json string\n");
        json_object_put(json);
        return NULL;
    }

    return json;
}

static void SetBaseURLInternal(char *url)
{
	base_url = url;
}


