################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../application/application.c \
../application/commandhandler.c \
../application/config.c \
../application/logic.c \
../application/parser.c \
../application/ui.c \
../application/userComm.c 

OBJS += \
./application/application.o \
./application/commandhandler.o \
./application/config.o \
./application/logic.o \
./application/parser.o \
./application/ui.o \
./application/userComm.o 

C_DEPS += \
./application/application.d \
./application/commandhandler.d \
./application/config.d \
./application/logic.d \
./application/parser.d \
./application/ui.d \
./application/userComm.d 


# Each subdirectory must supply rules for building sources it contributes
application/%.o: ../application/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


