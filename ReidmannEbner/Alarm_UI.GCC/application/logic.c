/*
 * logic.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "../global_defines.h"
#include "../interfaces/ilogic.h"
#include "../interfaces/icommunication.h"

#include "logic.h"

static struct ICommunication *_comm = NULL;

static bool ArmSystemInternal(char *password);
static bool ArmSnoozeInternal(char *password);
static bool DisarmSystemInternal(char *password);
static bool DisarmSnoozeInternal(char *password);
static bool CheckMotionInternal(void);
static bool CheckDisarmInternal(void);
static bool CheckArmSnoozeInternal(void);
static bool CheckDisarmSnoozeInternal(void);

static bool CleanupTimersInternal(char *password);
static bool CheckConnectionInternal();
//Implementation
struct ILogic * newILogic(struct ICommunication *comm)
{
	_comm = comm;

	struct ILogic *newStruct = malloc(sizeof(struct ILogic));
    if(newStruct == NULL)
    	return NULL;

    newStruct->ArmSystem = ArmSystemInternal;
	newStruct->ArmSnooze = ArmSnoozeInternal;
	newStruct->DisarmSystem = DisarmSystemInternal;
	newStruct->DisarmSnooze = DisarmSnoozeInternal;
	newStruct->CheckMotion = CheckMotionInternal;
	newStruct->CheckDisarm = CheckDisarmInternal;
	newStruct->CheckArmSnooze = CheckArmSnoozeInternal;
	newStruct->CheckDisarmSnooze = CheckDisarmSnoozeInternal;
	newStruct->CheckConnection = CheckConnectionInternal;


	newStruct->CleanupTimers = CleanupTimersInternal;

    return newStruct;
}

bool ArmSystemInternal(char *password)
{
	if (password == NULL)
		return false;

	return _comm->ArmTimer("Activation", password);
}

bool ArmSnoozeInternal(char *password)
{
	if (password == NULL)
		return false;

	return _comm->TriggerTimer("Activation", password);
}

bool DisarmSystemInternal(char *password)
{
	if (password == NULL)
		return false;

	bool resArm = _comm->DisarmTimer("Activation", password);
	bool resDisarm = _comm->DisarmTimer("Deactivation", password);
	return resDisarm && resArm;

}

bool DisarmSnoozeInternal(char *password)
{
	if (password == NULL)
		return false;

	return _comm->TriggerTimer("Deactivation", password);
}

bool CleanupTimersInternal(char *password)
{
	if (password == NULL)
		return false;

	bool resArm = _comm->DeleteTimer("Activation", password);
	bool resDisarm = _comm->DeleteTimer("Deactivation", password);

	return resDisarm && resArm;

}

bool CheckMotionInternal(void)
{
	return _comm->ReadMotion();
}

bool CheckDisarmInternal(void)
{
	return _comm->ReadButton();
}

bool CheckArmSnoozeInternal(void)
{
	return _comm->GetTimer("Activation");
}

bool CheckDisarmSnoozeInternal(void)
{
	return _comm->GetTimer("Deactivation");
}

bool CheckConnectionInternal()
{
	return _comm->CheckConnection();
}
