/*
 * parser.h
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#ifndef APPLICATION_PARSER_H_
#define APPLICATION_PARSER_H_


struct iCommandHandler;
struct iParser;


struct iParser* NewIParser(struct iCommandHandler* commandhandler);

#endif /* APPLICATION_PARSER_H_ */
