/*
 * UI.h
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#ifndef APPLICATION_UI_H_
#define APPLICATION_UI_H_

#include <stdbool.h>

struct iUI;
struct iParser;


struct iUI* NewIUI(struct iParser* parser);


#endif /* APPLICATION_UI_H_ */

