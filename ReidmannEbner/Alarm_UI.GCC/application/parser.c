/*
 * parser.c
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */



#include "../interfaces/iparser.h"
#include "../interfaces/icommandhandler.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>



static bool ParseInputInternal(char* input);



struct iCommandHandler* _commandhandler;


struct iParser* NewIParser(struct iCommandHandler* commandhandler){
	if(commandhandler == NULL){
		return NULL;
	}
	_commandhandler = commandhandler;
	struct iParser* new_parser = malloc(sizeof(iParser));
	if(new_parser == NULL){
		return NULL;
	}

	new_parser->ParseInput = ParseInputInternal;

	return new_parser;
}



bool ParseInputInternal(char* input){
	const struct Command* commands = _commandhandler->GetCommands();
	const int nr_of_commands = _commandhandler->GetNrOfCommands();
	commandtype selected_command = UNKNOWN;
	for(int i = 0; i < nr_of_commands; i++){
		if(strncmp(commands[i].commandname_, input, strlen(commands[i].commandname_)) == 0)
		{
			selected_command = commands[i].commandtype_;
			break;
		}
	}
	if(selected_command == UNKNOWN){
		return false;
	}


	//Check if there are arguments
	if(strlen(commands[selected_command].commandname_) != (strlen(input) - 1)){ //-1 because of the newline
		return false;
	}

	return _commandhandler->HandleCommand(selected_command);
}
