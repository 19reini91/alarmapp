/*
 * commandhandler.h
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#ifndef APPLICATION_COMMANDHANDLER_H_
#define APPLICATION_COMMANDHANDLER_H_



struct iCommandHandler* NewICommandHandler(struct ILogic* logic, struct IConfig* config);


#endif /* APPLICATION_COMMANDHANDLER_H_ */
