#include "../application/userComm.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "userComm.h"

#include "../interfaces/iuserComm.h"


static void ShowFailedFredosarConnectionInternal();

struct iuserComm* NewIUserComm(){
	struct iuserComm* new_userComm;
	new_userComm = malloc(sizeof(struct iuserComm));
	if(new_userComm == NULL){
		return NULL;
	}
	new_userComm->ShowFailedFredosarConnection = ShowFailedFredosarConnectionInternal;

	return new_userComm;
}


void ShowFailedFredosarConnectionInternal(){
	printf("ATTENTION: The connection to Fredosar was interrupted! The motion detection is currently not active.\n");
}

