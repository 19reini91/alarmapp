/*
 * commandhandler.c
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <unistd.h>
#include <limits.h>

#include "../interfaces/ilogic.h"
#include "../interfaces/iconfig.h"

#include "commandhandler.h"

#include "../interfaces/icommandhandler.h"
#include "../interfaces/iparser.h"
#include "../interfaces/iui.h"

static bool HandleCommandInternal(commandtype sel_command);
static bool setUIInternal(struct iUI* ui);
static const struct Command* GetCommandsInternal();
static const int GetNrOfCommandsInternal();


static bool Help();
static bool Quit();


static bool CheckConnection();
static bool SetArmTime();
static bool SetDisarmTime();
static bool ShowSettings();
static bool StartAlarm();



static struct iUI* _ui;
static struct ILogic* _logic;
static struct IConfig* _config;

static const int NR_OF_COMMANDS = UNKNOWN; //
static const struct Command commands[] = { {"start", START_ALARM, "Start the alarm", StartAlarm},
									{"check", CHECK_CONNECTION, "checks the connection to Fredosar", CheckConnection},
									{"setarm", SET_SNOOZE_TIME_ARM, "sets the snooze timers for arming", SetArmTime},
									{"setdisarm", SET_SNOOZE_TIME_DISARM, "sets the snooze timers for disarming", SetDisarmTime},
									{"showsettings", SHOW_SETTINGS, "shows the current settings", ShowSettings},
									{"help", HELP, "for help", Help},
									{"quit", QUIT, "to exit", Quit}
};



struct iCommandHandler* NewICommandHandler(struct ILogic* logic, struct IConfig* config)
{
	if(logic == NULL || config == NULL){
		return NULL;
	}
	_logic = logic;
	_config = config;

	struct iCommandHandler* new_command_handler = malloc(sizeof(iCommandHandler));
	if(new_command_handler == NULL)
	{
		return NULL;
	}
	new_command_handler->HandleCommand = HandleCommandInternal;
	new_command_handler->setUI = setUIInternal;
	new_command_handler->GetNrOfCommands = GetNrOfCommandsInternal;
	new_command_handler->GetCommands = GetCommandsInternal;

	return new_command_handler;
}

bool HandleCommandInternal(commandtype sel_command)
{
	for(int i = 0; i < NR_OF_COMMANDS; i++)
	{
		if(commands[i].commandtype_ == sel_command)
		{

			commands[i].Execute();
			return true;
		}
	}
	return false;
}

bool setUIInternal(struct iUI* ui)
{
	_ui = ui;
	return (_ui != NULL);
}

const struct Command* GetCommandsInternal(){
	return commands;
}
const int GetNrOfCommandsInternal(){
	return NR_OF_COMMANDS;
}

bool Help(){
	assert((_ui != NULL) && "You have to set the ui");
	_ui->ShowHelpMessage(commands, NR_OF_COMMANDS);
	return true;
}

bool Quit(){
	assert(_ui && "You have to set the ui");
	_ui->Stop();
	return true;
}


bool CheckConnection(){
	_ui->ShowMessage("Checking connection . . .");
	bool connected = _logic->CheckConnection();
	char msg[100];
	snprintf(msg, 100 - 1, "\nConnection %s established", connected? "": "NOT");
	_ui->ShowMessage(msg);
	return connected;
}

bool SetArmTime() {
	int time;
	_ui->ShowMessage("Enter new arm time (sec): ");
	if(_ui->ReadNumber(&time)){
		_config->SetArmSnooze(time);
		return true;
	}
	return false;
}

bool SetDisarmTime() {
	int time;
	_ui->ShowMessage("Enter new disarm time (sec): ");
	if(_ui->ReadNumber(&time)){
		_config->SetDisarmSnooze(time);
		return true;
	}
	return false;
}


bool ShowSettings(){
	char buffer[100];
	snprintf(buffer, 100 - 1, "Arm snooze time:\t %d sec \nDisarm snooze time: \t %d sec \n", _config->GetArmSnooze(), _config->GetDisarmSnooze());
	_ui->ShowMessage(buffer);
	return true;
}

bool StartAlarm(){
	_ui->StartAlarm();
	return true;
}


/**

bool ReadSocket(){
	bool socket_on = _comm->ReadSocket();
	_ui->ShowMessage(socket_on ? "\nSocket: ON" : "\nSocket: OFF");
	return socket_on;
}

bool Monitor(){
	while(!_ui->CheckKeyHit()){
		_ui->ShowMessage("\n---------------\n");
		_comm->ReadButton();
		_comm->ReadMotion();
		_comm->ReadSocket();
		usleep(500000);
	}
	_ui->ShowMessage("\n");
	return true;
}

bool ToggleSocket(){
	bool socket_state = _comm->ReadSocket();
	_comm->ControlSocket(!socket_state);
	_ui->ShowMessage(socket_state ? ("Socket set to OFF") : ("Socket set to ON"));
	return !socket_state;
}

**/

