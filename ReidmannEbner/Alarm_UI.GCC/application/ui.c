/*
 * UI.c
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */



#include "../application/ui.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include "ui.h"

#include "../interfaces/iui.h"

#include "../interfaces/icommandhandler.h"
#include "../interfaces/iparser.h"


#include "../helpers/kbhit.h"





static void ShowWelcomeMessageInternal();
static void ShowHelpMessageInternal(const struct Command* commands, int nr_of_commands);
static void ShowMessageInternal(char* msg);
static bool ReadStringInternal(char* buffer, int buffer_size);
static bool ReadNumberInternal(int* number);



static bool QuitRequestedInternal();
static bool AlarmStartRequestedInternal();


static void StartAlarmInternal();
static void StopInternal();
static bool ReadUserInputInternal();
static bool CheckKeyHitInternal();


static void ShowPrompt();
static void ShowUnknownMessage(char* entered_command);
static void ShowGoodBye();




static struct iParser* _parser;
static bool _running;
static bool _start_alarm;


struct iUI* NewIUI(struct iParser* parser){
	_running = true;
	_start_alarm = false;
	_parser = parser;
	if(parser == NULL){
		return NULL;
	}
	struct iUI* new_ui;
	new_ui = malloc(sizeof(struct iUI));
	if(new_ui == NULL){
		return NULL;
	}

	new_ui->ShowHelpMessage = ShowHelpMessageInternal;
	new_ui->ShowWelcomeMessage = ShowWelcomeMessageInternal;
	new_ui->ReadUserInput = ReadUserInputInternal;
	new_ui->CheckKeyHit = CheckKeyHitInternal;
	new_ui->Stop = StopInternal;
	new_ui->ShowMessage = ShowMessageInternal;
	new_ui->QuitRequested = QuitRequestedInternal;
	new_ui->ReadString = ReadStringInternal;
	new_ui->ReadNumber = ReadNumberInternal;
	new_ui->StartAlarm = StartAlarmInternal;
	new_ui->AlarmStartRequested = AlarmStartRequestedInternal;

	return new_ui;
}



bool ReadUserInputInternal(){
	ShowPrompt();
	char* buffer = NULL;
	size_t len = 0;
	getline(&buffer, &len, stdin);
	if(buffer == NULL){
		printf("ERROR\n");
		return false;
	}
	bool valid = _parser->ParseInput(buffer);
	if(!valid){
		ShowUnknownMessage(buffer);
	}

	free(buffer);
	return valid;
}

void ShowWelcomeMessageInternal(){
	printf("Welcome to the AlarmApp\n type \"start\" to start the alarm, or \"help\" for more information\n");
}

void ShowHelpMessageInternal(const struct Command* commands, int nr_of_commands){
	printf("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n");
	printf("Enter one of the following Commands: \n");
	for(int i = 0; i < nr_of_commands; i++)
	{
		printf("%s - %s \n", commands[i].commandname_, commands[i].description_);
	}
	printf("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-\n");
	printf("The Button can be used to disarm the system\n");
}


void ShowMessageInternal(char* msg){
	printf("%s\n",msg);
}

bool CheckKeyHitInternal(){
	int bytes_waiting = kbhit();
	if(bytes_waiting){
		char* buf = malloc(sizeof(char)*bytes_waiting);
		if(buf == NULL){
			return true;
		}
		read(STDIN_FILENO, buf, bytes_waiting);
		free(buf);
		buf = NULL;
	}
	return (bool)bytes_waiting;
}

void StopInternal(){
	ShowGoodBye();
	_running = false;
}

void ShowGoodBye(){
	printf("Bye!\n");
}

void ShowUnknownMessage(char* entered_command){
	printf("Unknown Command :%s  \n type \"help\" \n", entered_command);
}


void ShowPrompt(){
	printf(">> ");
	fflush(stdin);
}

bool QuitRequestedInternal(){
	return !_running;
}


bool ReadStringInternal(char* buffer, int buffer_size){
	char* tmp_buffer = NULL;
	bool success = true;
	size_t tmp_buffer_size = 0;
	int ret_val = getline(&tmp_buffer, &tmp_buffer_size, stdin);
	if(ret_val == -1){
		printf("An Error occured\n");
		success = false;
	}else if(buffer_size < tmp_buffer_size){
		printf("User Input is too long");
		success = false;
	}else{
		strncpy(buffer, tmp_buffer, tmp_buffer_size);
		buffer[strlen(buffer)] = '\0';
		printf("%s", buffer);
	}
	free(tmp_buffer);
	return success;

}


bool ReadNumberInternal(int* number){
	char c = '\0';
	if(scanf("%d%c", number, &c) < 0 || (c != '\n')){
		char* buffer = NULL;
		size_t size;
		getline(&buffer, &size, stdin);
		free (buffer);
		printf("Invalid Input\n");
		return false;
	}
	return true;
}

void StartAlarmInternal(){
	_start_alarm = true;
}

bool AlarmStartRequestedInternal(){
	return _start_alarm;
}






