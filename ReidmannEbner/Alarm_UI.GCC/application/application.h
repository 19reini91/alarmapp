/*
 * application.h
 *
 */

#ifndef APPLICATION_APPLICATION_H_
#define APPLICATION_APPLICATION_H_

bool ApplicationFactory(char *baseUrl, struct ILogic **logic, struct IConfig **config, struct iUI **ui, struct iuserComm **userComm);

void DisposeApplication(void);

#endif /* APPLICATION_APPLICATION_H_ */
