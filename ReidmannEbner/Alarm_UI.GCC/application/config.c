/*
 * config.c
 *
 *  Created on: 11.04.2019
 *      Author: pi
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>

#include "../global_defines.h"
#include "../interfaces/icommunication.h"
#include "../interfaces/iconfig.h"

#include "config.h"

static struct ICommunication *_comm = NULL;

static char *_password = NULL;
static int _arm_snooze = 0;
static int _disarm_snooze = 0;

static bool SetPasswordInternal(char *password);
static bool SetDisarmSnoozeInternal(int time);
static bool SetArmSnoozeInternal(int time);

static int GetArmSnoozeInternal();
static int GetDisarmSnoozeInternal();

//Implementation
struct IConfig * newIConfig(struct ICommunication *comm)
{
	_comm = comm;

	struct IConfig *newStruct = malloc(sizeof(struct IConfig));
    if(newStruct == NULL)
    	return NULL;

    newStruct->SetPassword = SetPasswordInternal;
    newStruct->SetDisarmSnooze = SetDisarmSnoozeInternal;
    newStruct->SetArmSnooze = SetArmSnoozeInternal;

    newStruct->GetArmSnooze = GetArmSnoozeInternal;
    newStruct->GetDisarmSnooze = GetDisarmSnoozeInternal;

    return newStruct;
}

static bool SetPasswordInternal(char *password)
{
	if (password == NULL)
		return false;
	_password = password;

	return true;
}

static bool SetDisarmSnoozeInternal(int time)
{
	_disarm_snooze = time;
	if (_password == NULL)
		return false;

	return _comm->SetTimer("Deactivation", _disarm_snooze, "Motion", "Socket", _password);
}

static bool SetArmSnoozeInternal(int time)
{
	_arm_snooze = time;
	if (_password == NULL)
		return false;

	return _comm->SetTimer("Activation", time, "Motion", "Socket", _password);
}

static int GetArmSnoozeInternal(){
	return _arm_snooze;
}
static int GetDisarmSnoozeInternal(){
	return _disarm_snooze;
}

