/*
 * EnergyConsumption.h
 *
 */

#ifndef ENERGYCONSUMPTION_H_
#define ENERGYCONSUMPTION_H_

struct IEnergyConsumption * newIEnergyConsumption(struct ICommunication *comm, struct ILogic *logic, struct IConfig *config, struct iUI  *ui);


bool ArmSnoozeInternal(char *password);

#endif /* ENERGYCONSUMPTION_H_ */