/*
 * EnergyConsumption.c
 *
 *  Created on: May 16, 2020
 *      Author: vmfredosar
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "../interfaces/icommunication.h"
#include "../interfaces/ilogic.h"
#include "../interfaces/iconfig.h"
#include "../interfaces/iui.h"
#include "../interfaces/ienergyconsumption.h"

#include "EnergyConsumption.h"

static struct ICommunication *_comm;
static struct ILogic *_logic;
static struct IConfig *_config;
static struct iUI*_ui;

static bool nightMode = false;
static bool receivedResponse = false;
static bool responseTimerExists = false;
static bool warningTimerExists = false;
static const char* responseTimerName = "ResponseTimer";
static const char* warningTimerName = "WarningTimer";

static bool getNightModeInternal();
static bool CheckEnergyConsumtionInternal();
static bool CheckTimerInternal(char* timerName);
static bool startTimerInternal(int maxTime, char* timerName, char* password);
static bool deleteTimerInternal(char* timerName, char* password);
static bool checkTimerValueInternal(char* timerName);
static bool checkDayTimeInternal();
static void sendWarningInternal(char* message);
static bool switchSocketInternal(bool state);
static void setNightModeInternal(bool on_off);
static void setReceivedResponseInternal(bool on_off);
static bool sendWarningRoutineInternal(bool day, char* password);


struct IEnergyConsumption * newIEnergyConsumption(struct ICommunication *comm, struct ILogic *logic, struct IConfig *config, struct iUI *ui){

	_comm = comm;
	_logic = logic;
	_config = config;
	_ui = ui;


	struct IEnergyConsumption *newStruct = malloc(sizeof(struct IEnergyConsumption));
    if(newStruct == NULL)
    	return NULL;

    newStruct->getNightMode = getNightModeInternal;
    newStruct->responseTimerName = responseTimerName;
    newStruct->warningTimerName = warningTimerName;
    newStruct->CheckEnergyConsumtion = CheckEnergyConsumtionInternal;
    newStruct->CheckTimer = CheckTimerInternal;
    newStruct->startTimer = startTimerInternal;
    newStruct->checkTimerValue = checkTimerValueInternal;
    newStruct->checkDayTime= checkDayTimeInternal;
    newStruct->sendWarning= sendWarningInternal;
    newStruct->switchSocket= switchSocketInternal;
    newStruct->deleteTimer= deleteTimerInternal;
    newStruct->setNightMode= setNightModeInternal;
    newStruct->setReceivedResponse= setReceivedResponseInternal;
    newStruct->sendWarningRoutine = sendWarningRoutineInternal;

    return newStruct;
}

bool getNightModeInternal()
{
	return nightMode;
}

bool CheckEnergyConsumtionInternal()
{
	return _comm->ReadSocket();
}

bool CheckTimerInternal(char* timerName)
{
	if(strcmp(timerName, responseTimerName) == 0)
	{
		return responseTimerExists;
	}
	if(strcmp(timerName, warningTimerName) == 0)
	{
		return warningTimerExists;
	}
	return false;
}

bool createTimer(int maxTime, char* timerName, char* password)
{
	_comm->SetTimer(timerName, maxTime, "SocketOn", "Socket", password);
//	if(!success)
//	{
//		return false;
//	}
	if(strcmp(timerName, responseTimerName) == 0)
	{
		responseTimerExists = true;
	}
	if(strcmp(timerName, warningTimerName) == 0)
	{
		warningTimerExists = true;
	}
	return true;
}

bool startTimerInternal(int maxTime, char* timerName, char* password)
{
	createTimer(maxTime, timerName, password);

	if(checkDayTimeInternal())
		sendWarningInternal("Time has started, we'll inform you in 2 hours if grandma is still cooking");

	return _comm->TriggerTimer(timerName, password);
}


bool checkTimerValueInternal(char* timerName)
{
	return _comm->GetTimer(timerName);
}

bool checkDayTimeInternal(){
	time_t rawTime;
	struct tm *timeInfo;
	time(&rawTime);
	timeInfo = localtime(&rawTime);
	int hour = timeInfo->tm_hour;
	if(hour < 7)
	{
		return false;
	}
	return true;
}

void sendWarningInternal(char* message)
{
	_ui->ShowMessage(message);
}

bool switchSocketInternal(bool state)
{
	if(!state)
	{
		_comm->ControlSocket(state);
		if(!CheckEnergyConsumtionInternal())
			return true;

		usleep(2000000);
		_comm->ControlSocket(state);
		if(!CheckEnergyConsumtionInternal())
			return true;

		sendWarningInternal("Emergency center is called");
		return false;
	}

	_comm->ControlSocket(state);
	if(CheckEnergyConsumtionInternal())
		return true;

	usleep(2000000);
	_comm->ControlSocket(state);
	if(CheckEnergyConsumtionInternal())
		return true;

	sendWarningInternal("oven could not be switched on");
	return false;

}

bool deleteTimerInternal(char* timerName, char* password)
{
	_comm->DeleteTimer(timerName, password);

	if(strcmp(timerName, responseTimerName) == 0)
	{
		responseTimerExists = false;
	}
	if(strcmp(timerName, warningTimerName) == 0)
	{
		warningTimerExists = false;
	}
	return true;
}

void setNightModeInternal(bool on_off)
{
	nightMode = on_off;
}

void setReceivedResponseInternal(bool on_off)
{
	receivedResponse = on_off;
}

bool sendWarningRoutineInternal(bool day, char* password)
{

	if(day || nightMode)
	{
		sendWarningInternal("Attention, your grandma is already using the oven since more than 2 hours. Send response within 15min (=15sek). To interrupt alarming process press any key");
	}
	else
	{
		sendWarningInternal("Attention, your grandma is using the oven during nighttime. Send response within 15min (=15sek). To interrupt alarming process press any key");
	}

	startTimerInternal(15, responseTimerName, password);


	while(!checkTimerValueInternal(responseTimerName) && receivedResponse == false)
	{
		setReceivedResponseInternal(_ui->CheckKeyHit());
	}

	deleteTimerInternal(responseTimerName, password);

	if(receivedResponse)
	{
		receivedResponse = false;
		if(!day && !nightMode)
		{
			setNightModeInternal(true);
			sendWarningInternal("Nightmode activated. We'll contact you again, if the oven is turned on for longer than 2 hours (=30s)");
		}
		else
		{
			sendWarningInternal("The oven will not be turned off for now, but we'll inform you again in 2 hours (=30s)");
			_comm->TriggerTimer(warningTimerName, password);
		}

		return true;
	}

	if(switchSocketInternal(false))
		sendWarningInternal("Grandma is save. Oven is turned off.");
	return false;
}
