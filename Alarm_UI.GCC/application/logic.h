/*
 * logic.h
 *
 */

#ifndef APPLICATION_LOGIC_H_
#define APPLICATION_LOGIC_H_

struct ILogic * newILogic(struct ICommunication *comm);

#endif /* APPLICATION_LOGIC_H_ */
