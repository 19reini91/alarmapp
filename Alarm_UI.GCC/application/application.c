/*
 * application.c
 *
 */

#include <stdbool.h>
#include <stdlib.h>
#include "../interfaces/icommunication.h"
#include "../interfaces/ilogic.h"
#include "../interfaces/iconfig.h"
#include "../interfaces/iui.h"
#include "../interfaces/iparser.h"
#include "../interfaces/icommandhandler.h"
#include "../EnergyConsumption/EnergyConsumption.h"


#include "../communication/commodule.h"

#include "ui.h"
#include "commandhandler.h"
#include "parser.h"
#include "logic.h"
#include "config.h"
#include "application.h"

static struct ICommunication *_comm;
static struct ILogic *_logic;
static struct IConfig *_config;
static struct iUI *_ui;
static struct iCommandHandler *_commandhandler;
static struct iParser* _parser;
static struct IEnergyConsumption* _energyConsumption;

bool ApplicationFactory(char *baseUrl, struct ILogic **logic, struct IConfig **config, struct iUI **ui, struct IEnergyConsumption **energyConsumption)
{
	if (baseUrl == NULL || logic == NULL || config == NULL || ui == NULL)
		return false;

	_comm = newICommunication(baseUrl);
	_logic = newILogic(_comm);
	_config = newIConfig(_comm);
	_commandhandler = NewICommandHandler(_logic, _config);
	_parser = NewIParser(_commandhandler);
	_ui = NewIUI(_parser);
	_commandhandler->setUI(_ui);
	_energyConsumption = newIEnergyConsumption(_comm, _logic, _config, _ui);

	*logic = _logic;
	*config = _config;
	*ui = _ui;
	*energyConsumption = _energyConsumption;

	return true;
}

void DisposeApplication(void)
{
	if (_config != NULL)
		free(_config);
	if (_logic != NULL)
		free(_logic);
	if (_ui != NULL)
		free(_ui);
	if(_commandhandler != NULL)
		free(_commandhandler);
	if(_parser != NULL)
		free(_parser);
	if (_comm != NULL) {
		_comm->Dispose();
		free(_comm);
	}
}
