/*
 * commodule.c
 *
 */

#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#include "../global_defines.h"

/* json-c (https://github.com/json-c/json-c) */
#include <json-c/json.h>

#include "../global_defines.h"
#include "../interfaces/icommunication.h"
#include "../interfaces/inetcom.h"
#include "../platform/restdriver.h"

#include "../communication/commodule.h"

//Declarations
static struct INetCom *_netCom = NULL;
static void ControlSocketInternal(bool bTurnOn);
static bool CheckConnectionInternal();

static bool ReadButtonInternal();
static bool ReadSocketInternal();
static bool ReadMotionInternal();

static bool SetParamInternal(char *param, char *value);
static const char *GetParamInternal(char *param);
static bool DeleteParamInternal(char *param);

static bool SetTimerInternal(char *timer, int time, char *trigger, char *action, char *password);
static bool GetTimerInternal(char *timer);
static bool ArmTimerInternal(char *timer, char *password);
static bool DisarmTimerInternal(char *timer, char *password);
static bool TriggerTimerInternal(char *timer, char *password);
static bool DeleteTimerInternal(char *timer, char *password);

static void DisposeInternal(void);


//Implementation
struct ICommunication * newICommunication(char *baseUrl)
{
	//obtain access to low level communication
	_netCom = newINetCom();
	_netCom->SetBaseURL(baseUrl);

	struct ICommunication *newStruct = malloc(sizeof(struct ICommunication));
    if(newStruct == NULL)
    	return NULL;

    newStruct->ControlSocket = ControlSocketInternal;
    newStruct->ReadButton = ReadButtonInternal;
    newStruct->ReadSocket = ReadSocketInternal;
    newStruct->ReadMotion = ReadMotionInternal;

    newStruct->SetParam = SetParamInternal;
    newStruct->GetParam = GetParamInternal;
    newStruct->DeleteParam = DeleteParamInternal;

    newStruct->SetTimer = SetTimerInternal;
    newStruct->GetTimer = GetTimerInternal;
    newStruct->ArmTimer = ArmTimerInternal;
    newStruct->DisarmTimer = DisarmTimerInternal;
    newStruct->TriggerTimer = TriggerTimerInternal;
    newStruct->DeleteTimer = DeleteTimerInternal;

    newStruct->CheckConnection = CheckConnectionInternal;

    newStruct->Dispose = DisposeInternal;

    return newStruct;
}

static void ControlSocketInternal(bool bTurnOn)
{
	char *msg = !bTurnOn ? "control?address=Socket&state=OFF" : "control?address=Socket&state=ON";
	_netCom->SendMessage(msg, "PUT", NULL);
}

static bool GetState(char *identifier, size_t array_idx, char *key, char *expected_val)
{
	if (identifier == NULL || key == NULL || expected_val == NULL)
		return false;

	char *msg = "state?identifier=";
	size_t buffer = strlen(msg) + strlen(identifier) + 1;

	char *state_msg = malloc(buffer);
	sprintf(state_msg, "%s%s", msg, identifier);

	json_object *state = _netCom->SendMessage(state_msg, "GET", NULL);
	free(state_msg);

	if (state == NULL)
		return false;

    if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object *obj_state = NULL;
	json_bool found = json_object_object_get_ex(state, key, &obj_state);
	bool bConfirmed = found && !strcmp(json_object_get_string(obj_state), expected_val);

	json_object_put(state);

	return bConfirmed;
}

static bool ReadButtonInternal()
{
	return GetState("Button", 0, "state", "PRESSED");
}

static bool ReadSocketInternal()
{
	return GetState("Socket", 0, "state", "ON");
}

static bool ReadMotionInternal()
{
	json_object *state = _netCom->SendMessage("state?identifier=Motion", "GET", NULL);

	if (state == NULL)
		return false;

	/* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object *obj_state = NULL;
	json_bool found = json_object_object_get_ex(state, "motionDetected", &obj_state);
	bool bMotionDetected = found && !strcmp(json_object_get_string(obj_state), "true");

	if (bMotionDetected) {
		found = json_object_object_get_ex(state, "lastMotion", &obj_state);
		if (found && obj_state != NULL) {
			char *ptr;
			const char *stringTime = json_object_get_string(obj_state);
			unsigned long long motionTime = strtoull(stringTime, &ptr, 10);
			long sysTime = (long)time(NULL);
			bMotionDetected = ((sysTime-motionTime/1000) < 5);
		}
	}

	json_object_put(state);

	return bMotionDetected;

}

static bool SetParamInternal(char *param, char *value)
{
	if (param == NULL || value == NULL)
		return false;

	char msg[256];
	sprintf(msg,"param?name=%s", param);

    /// create json object for post
    json_object *json_post = json_object_new_object();
    // build post data
    json_object_object_add(json_post, "value", json_object_new_string(value));

	json_object *state = _netCom->SendMessage(msg, "POST", json_post);

	if (state == NULL)
		return false;

    /* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object_put(state);

	return true;
}

static const char *GetParamInternal(char *param)
{
	if (param == NULL)
		return false;

	char msg[256];
	sprintf(msg,"param?name=%s", param);

	json_object *state = _netCom->SendMessage(msg, "GET", NULL);

	if (state == NULL)
		return NULL;

	const char *value = json_object_to_json_string(state);

    /* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", value);

	json_object_put(state);

	return value;
}

static bool DeleteParamInternal(char *param)
{
	if (param == NULL)
		return false;

	char msg[256];
	sprintf(msg,"param?name=%s", param);

	json_object *state = _netCom->SendMessage(msg, "D", NULL);

	if (state == NULL)
		return false;

    /* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object_put(state);

	return true;
}


static bool SetTimerInternal(char *timer, int time, char *trigger, char *action, char *password)
{
	if (timer == NULL || password == NULL || trigger == NULL || action == NULL)
		return false;

	char state_msg[256];
	sprintf(state_msg, "timer?name=%s&snooze_time=%d&trigger=%s&action=%s&state=DISARMED&password=%s", timer, time, trigger, action, password);

	json_object *state = _netCom->SendMessage(state_msg, "POST", NULL);

	if (state == NULL)
		return false;

	json_object_put(state);

	return true;
}

static bool GetTimerInternal(char *timer)
{
	if (timer == NULL)
		return false;

	char state_msg[256];
	sprintf(state_msg, "timer?name=%s", timer);

	json_object *state = _netCom->SendMessage(state_msg, "GET", NULL);

	if (state == NULL)
		return false;

    /* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object *obj_state = NULL;

	json_bool found = json_object_object_get_ex(state, "alarm_time", &obj_state);
	bool bTriggered = false;
	//Check if there is a formatted Timestamp for the trigger time and if the trigger event already lies in the past
	if (found && obj_state != NULL)
	{
		struct tm tm = {0};
		strptime(json_object_get_string(obj_state), "%Y-%m-%dT%T.%f%Z", &tm);
		time_t triggerTime = mktime(&tm);
		//triggerTime = mktime(gmtime(&triggerTime));
		time_t sysTime = time(NULL);
		sysTime = mktime(localtime(&sysTime));

		bTriggered = (sysTime+3600)>triggerTime;
	}

	json_object_put(state);

	return bTriggered;
}

static bool ControlTimer(char *control, char *timer, char *password)
{
	if (control == NULL || timer == NULL || password == NULL)
		return false;

	char state_msg[256];
	sprintf(state_msg, "%s?name=%s&password=%s", control, timer, password);

	json_object *state = _netCom->SendMessage(state_msg, "PUT", NULL);

	if (state == NULL)
		return false;

    /* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object_put(state);

	return true;
}

static bool ArmTimerInternal(char *timer, char *password)
{
	return ControlTimer("armTimer", timer, password);
}

static bool DisarmTimerInternal(char *timer, char *password)
{
	return ControlTimer("disarmTimer", timer, password);
}

static bool TriggerTimerInternal(char *timer, char *password)
{
	return ControlTimer("triggerTimer", timer, password);
}


static bool DeleteTimerInternal(char *timer, char *password)
{
	if (timer == NULL || password == NULL)
		return false;

	char state_msg[256];
	sprintf(state_msg, "timer?name=%s&password=%s", timer, password);

	json_object *state = _netCom->SendMessage(state_msg, "DELETE", NULL);

	if (state == NULL)
		return false;

    /* debugging */
	if (g_verbose) printf("Parsed JSON: %s\n", json_object_to_json_string(state));

	json_object_put(state);

	return true;
}


static void DisposeInternal(void){
	if (_netCom != NULL)
		free(_netCom);
}

static bool CheckConnectionInternal(){
	char state_msg[256];
	json_object *state = _netCom->SendMessage("healthy", "GET", NULL);
	if(state == NULL){
		return false;
	}


	json_object_put(state);
	return true;
}
