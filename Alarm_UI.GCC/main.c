/*
 * main.c
 *
 */

/* standard includes */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include <termio.h>
#include <unistd.h>
#include <time.h>

#include "interfaces/iconfig.h"
#include "interfaces/ilogic.h"
#include "interfaces/iui.h"
#include "interfaces/ienergyconsumption.h"

#include "application/application.h"

#include "helpers/kbhit.h"

#define REDIRECTERROR //Comment this line to show error messages, otherwise they are stored in errors.txt

#define MESSAGELENGTH 256

/* Echo Communication Messages to console*/
bool g_verbose = false;

int main(int argc, char *argv[])
{

#ifdef REDIRECTERROR
	FILE* error_file = freopen("alarmerrors.txt", "w", stderr); // <- redirects error messages to errors.txt"
	time_t current_time = time(NULL);
	fprintf(error_file, "----------------%s------------- \n \n \n", ctime(&current_time));
#endif //REDIRECTERROR

	struct ILogic *logic = NULL;
	struct IConfig *config = NULL;
	struct iUI *ui  = NULL;
	struct IEnergyConsumption *energyConsumption = NULL;

	char msg[MESSAGELENGTH];

	if(!ApplicationFactory("http://localhost:8080/shapi/", &logic, &config, &ui, &energyConsumption)){
		return -1;
	}

	char *password = "swordfish";



    if(!logic->CheckConnection()){
    	ui->ShowMessage("You have to start FREDOSAR!\n ");
    	ui->ShowMessage("Waiting for FREDOSAR ... ");
        do{
        	sleep(1);
        }while(!logic->CheckConnection());
        ui->ShowMessage("Connected!");
    }


    ui->ShowMessage("Configuring System");
	logic->CleanupTimers(password);
	config->SetPassword(password);

	ui->ShowWelcomeMessage();

	bool alarm_started = false;
	bool quit_requested = false;

	do{
		ui->ReadUserInput();
		alarm_started = ui->AlarmStartRequested();
		quit_requested = ui->QuitRequested();
		}while(!alarm_started && !quit_requested);

	ui->ShowMessage("As soon as grandma turns on the oven (Socket is ON!), the application will start monitoring the duration of her oven usage");
	while(1)
	{
		if(!energyConsumption->CheckEnergyConsumtion())
			continue;

		if(!energyConsumption->CheckTimer(energyConsumption->warningTimerName))
		{
			energyConsumption->startTimer(30, energyConsumption->warningTimerName, password);

			if(energyConsumption->checkDayTime() || energyConsumption->getNightMode())
				continue;

			if(energyConsumption->sendWarningRoutine(energyConsumption->checkDayTime(), password))
				continue;
			else
				break;
		}

		if(energyConsumption->checkTimerValue(energyConsumption->warningTimerName))
		{
			if(energyConsumption->sendWarningRoutine(energyConsumption->checkDayTime(), password))
				continue;
			else
				break;
		}
		if(energyConsumption->checkDayTime() || energyConsumption->getNightMode())
			continue;

		if(energyConsumption->sendWarningRoutine(energyConsumption->checkDayTime(), password))
			continue;
		else
			break;
	}


	if(quit_requested){
		DisposeApplication();
	}



	logic->CleanupTimers(password);

	ui->ShowMessage("Terminating App ...\n");

	DisposeApplication();


#ifdef REDIRECTERROR
	fclose(error_file);
#endif //REDIRECTERROR


    return 0;
}

