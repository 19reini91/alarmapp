/*
 * commandhandler.h
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#ifndef INTERFACE_COMMANDHANDLER_H_
#define INTERFACE_COMMANDHANDLER_H_


#include "iui.h"




typedef enum {START_ALARM = 0, CHECK_CONNECTION, SET_SNOOZE_TIME_ARM, SET_SNOOZE_TIME_DISARM, SHOW_SETTINGS, HELP, QUIT, UNKNOWN} commandtype; //UNKNOWN MUST be the last entry.

///Struct that represents a command
struct Command{
	char commandname_[20]; ///Name of the command
	commandtype commandtype_; ///commandtype
	char description_[100]; ///Description of the command (shown in help message)
	bool (*Execute)(); ///Function that the command should execute
}Command;


struct iCommandHandler{
	bool (*HandleCommand)(commandtype sel_command);
	bool (*setUI)(struct iUI* ui);
	const struct Command* (*GetCommands)();
	const int (*GetNrOfCommands)();
}iCommandHandler;


#endif /* INTERFACE_COMMANDHANDLER_H_ */
