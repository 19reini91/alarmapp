/*
 * UI.h
 *
 *  Created on: 07.07.2019
 *      Author: pi
 */

#ifndef INTERFACE_UI_H_
#define INTERFACE_UI_H_

#include <stdbool.h>

struct Command;

struct iUI{
	void (*ShowMessage)(char* msg);
	void (*ShowWelcomeMessage)();
	void (*ShowHelpMessage)(const struct Command* commands, int nr_of_commands);
    void (*Stop)();
    bool (*QuitRequested)();
    void (*StartAlarm)();
    bool (*AlarmStartRequested)();
    bool (*ReadUserInput)();
    bool (*ReadString)(char* buffer, int buffer_size);
    bool (*ReadNumber)(int* number);
    bool (*CheckKeyHit)();
}iUI;


#endif /* INTERFACE_UI_H_ */

