/*
 * iconfig.h
 *
 */

#ifndef INTERFACES_ICONFIG_H_
#define INTERFACES_ICONFIG_H_


struct IConfig
{
    bool (*SetPassword)(char *);
    bool (*SetDisarmSnooze)(int);
    bool (*SetArmSnooze)(int);
    bool (*SetNoMotionSnooze)(int);

    int (*GetArmSnooze)();
    int (*GetDisarmSnooze)();
    int (*GetNoMotionSnooze)();

} IConfig;


#endif /* INTERFACES_ICONFIG_H_ */
