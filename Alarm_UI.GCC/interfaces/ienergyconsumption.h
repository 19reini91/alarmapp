/*
 * ienergyconsumption.h
 *
 *  Created on: May 16, 2020
 *      Author: vmfredosar
 */

#ifndef INTERFACES_IENERGYCONSUMPTION_H_
#define INTERFACES_IENERGYCONSUMPTION_H_

#include <stdbool.h>

struct IEnergyConsumption
{

	bool (*getNightMode)();
	char* (*responseTimerName);
	char* (*warningTimerName);
	bool (*CheckEnergyConsumtion)();
	bool (*CheckTimer)(char* timerName);
	bool (*startTimer)(int maxTime, char* timerName, char* password);
	bool (*deleteTimer)(char* timerName, char* password);
	bool (*checkTimerValue)(char* timerName);
	bool (*checkDayTime)();
	void (*sendWarning)(char* message);
	bool (*switchSocket)(bool state);
	void (*setNightMode)(bool on_off);
	void (*setReceivedResponse)(bool on_off);
	bool (*sendWarningRoutine)(bool day, char* password);

} IEnergyConsumption;

#endif /* INTERFACES_IENERGYCONSUMPTION_H_ */
