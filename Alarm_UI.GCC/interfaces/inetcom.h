/*
 * inetcom.h
 *
 */

#ifndef INTERFACES_INETCOM_H_
#define INTERFACES_INETCOM_H_


struct INetCom
{
    json_object *(*SendMessage)(char *msg, char *request, json_object *post);
    void (*SetBaseURL)(char *url);

} INetCom;


#endif /* INTERFACES_INETCOM_H_ */
