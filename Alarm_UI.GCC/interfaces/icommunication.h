/*
 * icommunication.h
 *
 */

#ifndef INTERFACES_ICOMMUNICATION_H_
#define INTERFACES_ICOMMUNICATION_H_


struct ICommunication
{
	void (*ControlSocket)(bool bTurnOn);
	bool (*CheckConnection)();

	bool (*SetParam)(char *param, char *value);
	const char *(*GetParam)(char *param);
	bool (*DeleteParam)(char *param);

    bool (*SetTimer)(char *timer, int time, char *trigger, char *action, char *password);
    bool (*GetTimer)(char *timer);
    bool (*ArmTimer)(char *timer, char *password);
    bool (*DisarmTimer)(char *timer, char *password);
    bool (*TriggerTimer)(char *timer, char *password);
    bool (*DeleteTimer)(char *timer, char *password);

    bool (*ReadMotion)(void);
    bool (*ReadButton)(void);
    bool (*ReadSocket)(void);

    void (*Dispose)(void);
} ICommunication;



#endif /* INTERFACES_ICOMMUNICATION_H_ */
